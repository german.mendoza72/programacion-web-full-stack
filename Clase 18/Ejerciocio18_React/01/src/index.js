import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

/*
Con React: convertir el ejercicio JS04 en un componente de React Menú y mostrarlo dentro de un componente App. 
Agregarle un componente Titular  y un componente Figura que tenga 3 imágenes y un epígrafe
*/

const Titular =(props)=>(
  <h1>{props.titular}</h1>)


const menu = [
  { texto: "Home", link: "index.html"},
  { texto: "Contacto", link: "contacto.html"},
  { texto: "Nosotros", link: "about.html"}
];

const Menu = () =>(
  menu.map((enlace,i)=><li key={i}><a href={"/" + enlace.link}> {enlace.texto}</a></li>)
  );

const Nav = () => (
  <ul>
    <Menu />
  </ul>
  )

const Figura = (props) =>(
    <>
    <figure>
        <img src={props.url1} alt={props.alter}/>
        <img src={props.url2} alt={props.alter}/>
        <img src={props.url3} alt={props.alter}/>
        <figcaption>{props.caption}</figcaption>
    </figure>
    </>
)

const App = () =>(
  <>
  <Titular titular="El Gran titulo."/>
  <nav><Nav /></nav>
  <Figura url1="https://via.placeholder.com/150/0000FF/808080" url2="https://via.placeholder.com/150/FF0000/FFFFFF" url3="https://via.placeholder.com/150/FFFF00/000000" alter="Las figuras" caption="Fotos de Prueba" />
      
   </>
 ) 

// ReactDOM.render inserta en componente App en el div id root que está en index.html de la carpeta public
ReactDOM.render(<App />,document.getElementById('root'));
