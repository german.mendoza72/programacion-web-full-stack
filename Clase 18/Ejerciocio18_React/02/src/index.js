import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

/*
Sin REACT. Mostrar en un HTML imagen (en un <img>) y título (en un <h1>) de 
const datos
Sin REACT.  Del JSON de películas que vimos en clase mostrar solo los actores en una lista (ul li).
Se puede con for of.
Sin REACT. Dado un JSON en una variable llamada estilos, aplicar esos estilos a un div cuya id es caja (sin usar for).
*/
const datos = {
  "albumId": 1,
    "id": 1,
    "title": "accusamus beatae ad facilis cum similique qui sunt",
    "url": "https://via.placeholder.com/600/92c952"
}
const peliculas = [
  { pelicula: "Inception", director: "Christopher Nolan", actor: "Leonardo DiCaprio" },
  { pelicula: "500 Days of Summer", director: "Marc Webb", actor: "Joseph Gordon-Levitt" },
  { pelicula: "Dark Shadows", director: "Tim Burton", actor: "Johnny Depp" },
  { pelicula: "Volver al Futuro", director: "Robert Zemeckis", actor: "Michael Fox"}			
  ];

const estilos = {backgroundColor: "blue",width: "450px",height:"700px"}
const styles = {
  caja: {
    width: estilos.width,
    height: estilos.height,
    backgroundColor:estilos.backgroundColor
  }
};

const Pelicula = () =>(
    peliculas.map((peli,i)=><li key={i}>{peli.actor}</li>)
  );  

const Titular =(props)=>(
  <h1>{props.titular}</h1>)

const App = () =>(
    <>
    <Titular titular="Titulo del Ejercicio 18-1,2 y 3"/>
    <nav>
          <ul><Pelicula /></ul>
    </nav>
    <h1> Titulo del Json : {datos.title}</h1>
    <div id="caja" style={styles.caja}></div>
    
    <img src={datos.url} alt="Imagen Json" />  
    
   </>
 ) 

 ReactDOM.render(<App />,document.getElementById('root'));
