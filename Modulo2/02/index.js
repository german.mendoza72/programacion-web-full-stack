import React from 'react'
import ReactDOM from "react-dom";
//import { useForm } from "react-hook-form";



const App = () => {
  const [text, setText ] = React.useState('');
  
  function handleChange(event){
    setText(event.target.value);
  }
  return (
    <main>
    <form onChange={handleChange}>
      <input name="firstName" onChange={handleChange} />
    </form>
    {text?<p>texto: {text}</p>:null}
    </main>
     
  );
  
}

ReactDOM.render(<App />, document.getElementById('root'))