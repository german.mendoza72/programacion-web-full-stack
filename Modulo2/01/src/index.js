import React, { useState } from 'react';
import ReactDOM from 'react-dom';

const Formulario = (props) => {
  const [fName, setfName] = useState();
  const [fApe, setfApe] = useState();
  const [fPais, setfPais] = useState("0");
  const [fNum, setfNum] = useState("0");
  const Enviar = () => {    
    setfName(document.getElementById(props.idnombre).value)
    setfApe(document.getElementById(props.idapellido).value)
    setfPais(document.getElementById(props.idpais).value)
    setfNum(document.getElementById(props.idnumero).value)
  }
  
  return(
  <main>
    <form>
      <input type="text" placeholder="Nombre"  id={props.idnombre} className="error"/>
      <input type="text" placeholder="Apellido"  id={props.idapellido} />  
      <label>Numero</label>
      <select  id={props.idnumero} >
          <option value="0">Elegir ....</option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
          <option value="7">7</option>
          <option value="8">8</option>
          <option value="9">9</option>
          <option value="10">10</option>
      </select>      
      <label>País</label>
      <select  id={props.idpais} >
          <option value="0">Elegir ....</option>
          <option value="AR">Argentina</option>
          <option value="BR">Brasil</option>
          <option value="CH">Chile</option>
      </select>
      <input type="button" onClick={Enviar} value="Enviar" />
    </form>
    {fName?<p>Nombre: {fName}</p>:null}
    {fApe?<p>Apellido: {fApe}</p>:null}
    {fPais!=="0"?<p>País: {fPais}</p>:null}
    {fNum !=="0"?<p>Numero: {fNum}</p>:null}
  </main>
  )
}

  ReactDOM.render(<Formulario idnombre="nom" idapellido="ape" idpais="pais" idnumero="num" />, document.getElementById('root')
);