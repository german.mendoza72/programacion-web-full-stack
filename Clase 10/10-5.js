/*
Dada la variable booleana encendido. Mostrar el contenido de la variable carga 
(número del 0 al 100) solo si encendido es true. Si carga vale menos de 20, 
agregar un mensaje que cargue el dispositivo (solo si encendido es true).

*/

var encendido=true;
var carga=10;

if ( encendido==true ){
    console.log("Encendido ok Muestra Valor Carga :",carga);
}
if ( encendido==true && carga < 20){
    console.log("Por favor realizar la carga el valor es menor a 20 :",carga);
}


