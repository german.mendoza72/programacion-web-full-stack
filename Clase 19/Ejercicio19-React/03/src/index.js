import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

/*
Usando React (sin hooks). Crear un JSON peliculas con la siguiente estructura 
[{titulo:“Inception”,”poster:”inception.jpg”,year:”2010},...] 
crear un componente que muestre las películas del año actual (el año obtenerlo con JavaScript).
Al menos 5 peliculas en el JSON.
*/
const peliculas = [
  { Titulo: "Inception", poster: "Inception.jpg", year: "2010" },
  { Titulo: "Inception1", poster: "Inception1.jpg", year: "2015" },
  { Titulo: "Inception2", poster: "Inception2.jpg", year: "2020" },
  { Titulo: "Inception3", poster: "Inception3.jpg", year: "2020" }			
  ];

var d = new Date();
var anio = d.getFullYear();
console.log(anio); 

const Pelicula = () =>(
peliculas.filter(peli=> parseInt(peli.year) ===anio).map((peli,i) => <li key={i}>Pelicula:{peli.Titulo}- poster: {peli.poster}</li>)
 ) 

const Titular =(props)=>(
  <h1 id="titulo">{props.titular}</h1>)

const App = () =>(
  <>
    <Titular titular="Titulo del Ejercicio 19-3 "/>
    <nav>
      <ul><Pelicula /></ul>

    </nav>
    
  </>
 ) 

 ReactDOM.render(<App />,document.getElementById('root'));
