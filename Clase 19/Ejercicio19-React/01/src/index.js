import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

/*
Usando React (sin hooks). Crear un JSON paises que tendrá países de América y de Europa con la siguiente estructura 
[{pais:“Argentina”,”capital:”Buenos Aires”,continente:”América”},...]
Crear un componente que muestre en una tabla con cabecera Países de América (Pais y Ciudad). 
Tiene un h1 el componente que sea América
Crear un componente que muestre en una tabla con cabecera Países de Europa (Pais y Ciudad). 
Tiene un h1 el componente que sea Europa.

*/
const paises =[{pais:"Argentina",capital:"Buenos Aires",continente:"America"},
{pais:"Francia",capital:"Paris",continente:"Europa"},
{pais:"Inglaterra",capital:"Londres",continente:"Europa"},
{pais:"Uruguay",capital:"Montivideo",continente:"America"},
{pais:"Peru",capital:"Lima",continente:"America"},
];

const America = () =>(
paises.filter(pais=> pais.continente ==="America").map((pais,i) => (<tr key={i}><td>{pais.pais}</td><td>{pais.capital}</td></tr>))
) 
const Europa = () =>( 
paises.filter(pais=> pais.continente ==="Europa").map((pais,i) =>(<tr key={i}><td>{pais.pais}</td><td>{pais.capital}</td></tr>))
)

const PaisesAmerica = () => (
<>
        <h1>Paises de America</h1>
        <table border="2">
            <thead>
                <tr>
                    <th>
                        Pais
                    </th>
                    <th>
                        Ciudad
                    </th>
                </tr>
            </thead>
            <tbody>                
                    <America/>                
            </tbody>
        </table>
    </>
)

const PaisesEuropa = () => (
  <>
          <h1>Paises de Europa</h1>
          <table border="2">
              <thead>
                  <tr>
                      <th>
                          Pais
                      </th>
                      <th>
                          Ciudad
                      </th>
                  </tr>
              </thead>
              <tbody>                
                      <Europa/>                
              </tbody>
          </table>
      </>
  )

const Titular =(props)=>(
  <h1 id="titulo">{props.titular}</h1>)

const App = () =>(
  <>
    <Titular titular="Titulo del Ejercicio 19-1 "/>
    <nav> 
                     
    <ul><PaisesAmerica /></ul>
    
    <ul><PaisesEuropa/></ul>

    </nav>
  </>
 ) 

 ReactDOM.render(<App />,document.getElementById('root'));
