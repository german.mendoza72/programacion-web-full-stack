import React, { useState } from 'react';
import ReactDOM from 'react-dom';

/*
Usando React y el Hook UseState. Al pulsar un botón que se muestre una pelicula random de un array.
*/

const peliculas = [
  { pelicula: "Inception", director: "Christopher Nolan", actor: "Leonardo DiCaprio" },
  { pelicula: "500 Days of Summer", director: "Marc Webb", actor: "Joseph Gordon-Levitt" },
  { pelicula: "Dark Shadows", director: "Tim Burton", actor: "Johnny Depp" },
  { pelicula: "Volver al Futuro", director: "Robert Zemeckis", actor: "Michael Fox"}			
  ];

  const Pelicula = () =>(
    peliculas.map((peli,i)=><li key={i}>{peli.pelicula}</li>)
  );  

  const useRandomPeli = () => {  
  const [peli, setPeli] = useState();
  const changePeli = () => {
  const index = Math.floor(Math.random() * peliculas.length);  
  const peliElegido = peliculas[index].pelicula
  setPeli(peliElegido)
};
return [peli, changePeli];
};

const Titular =(props)=>(
  <h1>{props.titular}</h1>)

//Arranco con llaves un componente cuando comienzo con JS puro
const PeliculaBanner = () => {    
const [peli, changePeli] = useRandomPeli();
return (
    <div >
      <Titular titular="Titulo del Ejercicio 19-2"/>
      <nav>
          <ul><Pelicula /></ul>
    </nav>
      <h2 >Esto es una prueba Random de Pelicula  : {peli}</h2>
      <br />
      <button onClick={changePeli}>Cambiar pelicula</button>
    </div>
  );
};

ReactDOM.render(<PeliculaBanner />, document.getElementById('root'));