import React from 'react';
import ReactDOM from 'react-dom';
import SimpleBoton from './Boton.js';
import './index.css';

/*
Crear un generador de claves (React + Material UI + Hook de estado)
El usuario tendrá un botón de Materia UI
Las claves serán random alfanuméricas de 12 caracteres.
Y se muestran en un párrafo.
*/
const App = () => (
  <>
  <h1>Ejercicio 20-1</h1>
  <SimpleBoton/>
  
  </>
)  

ReactDOM.render(<App />, document.getElementById('root'));