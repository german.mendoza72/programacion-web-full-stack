import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';


    function generar(){
      const alfanum = "ABCDEFGHYJKLMNÑOPQRSTUVWXYZabcdefghyjklmnñopqrstuvwxyz0123456789#!*"
      var caracteres = alfanum.length;
      var acumula = ""
      for(var i=0;i<=11;i++){
            acumula+= alfanum[Math.floor(Math.random()*caracteres)]
      }            
    // console.log(acumula)  
      return(
          acumula
      )
    }
    //console.log(generar()) 
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
}));

export default function ContainedButtons() {
  const classes = useStyles();
  const [count, setCount] = React.useState(0);
  const Mostrar = () =>count ? <h2>Generar Clave: {generar()}</h2>:null
  return (
   <div className={classes.root}>
  
  <Button variant="contained" color="primary" onClick={() => setCount(count + 1)}>
      Generar Clave  
    </Button>
    <Mostrar/>
     
    </div>
  );
}
